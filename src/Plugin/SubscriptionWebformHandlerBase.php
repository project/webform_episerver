<?php

namespace Drupal\webform_episerver\Plugin;

use Drupal\Core\Form\FormStateInterface;
use Drupal\webform\Plugin\WebformHandlerBase;
use Drupal\webform\WebformSubmissionInterface;
use Drupal\webform_episerver\WebFormEpiserverElementsMappingTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class SubscriptionWebformHandlerBase.
 *
 * Provides the base Webform Handler to implement the integration with
 * Episerver.
 */
abstract class SubscriptionWebformHandlerBase extends WebformHandlerBase {
  use WebFormEpiserverElementsMappingTrait;

  /**
   * The WebformEpiserverManagerInterface service.
   *
   * @var \Drupal\webform_episerver\WebformEpiserverManagerInterface
   */
  protected $webformEpiserverManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->webformEpiserverManager = $container->get('webform_episerver.manager');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'auth_code' => '',
      'opt_id_status' => FALSE,
      'opt_id' => '',
      'email' => '',
      'trigger_by_checkbox' => FALSE,
      'trigger_checkbox_elem' => '',
      'opt_in_source' => '',
      'default_saludation' => (string) $this->t('Default saludation'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form['mail_list'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Mail list settings'),
    ];

    $form['mail_list']['auth_code'] = [
      '#type' => 'key_select',
      '#title' => $this->t('Auth code'),
      '#default_value' => $this->configuration['auth_code'],
      '#required' => TRUE,
    ];

    $form['mail_list']['opt_in_source'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Opt-in Source'),
      '#description' => $this->t('Optional parameter that defines the opt-in source of the recipient. For instance "Website"'),
      '#default_value' => $this->configuration['opt_in_source'],
    ];

    $form['mail_list']['opt_id_status'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable Opt-id'),
      '#description' => $this->t(
        'If checked, when a user is about to be subscribed, the Opt-in ID
        will be sent to the Episerver instance to trigger the configured Double
        Opt-in workflow.'
      ),
      '#default_value' => $this->configuration['opt_id_status'],
    ];

    $form['mail_list']['opt_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Opt-in ID'),
      '#description' => $this->t('The Opt-in ID configured at the Episerver instance.'),
      '#default_value' => $this->configuration['opt_id'],
      '#states' => [
        'visible' => [
          ':input[name="settings[mail_list][opt_id_status]"]' => [
            'checked' => TRUE,
          ],
        ],
        'required' => [
          ':input[name="settings[mail_list][opt_id_status]"]' => [
            'checked' => TRUE,
          ],
        ],
      ],
    ];

    $form['mail_list']['trigger_by_checkbox'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Only subscribe the user if when checks a confirmation checkbox.'),
      '#description' => $this->t(
        'If checked, the subscription will only be run if the user checks the configured checkbox element. Otherwise, the subscription process will be always triggered.'
      ),
      '#default_value' => $this->configuration['trigger_by_checkbox'],
    ];

    $form['mail_list']['trigger_checkbox_elem'] = [
      '#type' => 'select',
      '#title' => $this->t('Confirmation checkbox'),
      '#description' => $this->t('The checkbox element which when checked the subscription process will be triggered.'),
      '#default_value' => $this->configuration['trigger_checkbox_elem'],
      '#options' => $this->getWebformElements(),
      '#states' => [
        'visible' => [
          ':input[name="settings[mail_list][trigger_by_checkbox]"]' => [
            'checked' => TRUE,
          ],
        ],
        'required' => [
          ':input[name="settings[mail_list][trigger_by_checkbox]"]' => [
            'checked' => TRUE,
          ],
        ],
      ],
    ];

    $form['elements_mapping'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Form elements mapping'),
    ];

    $form['elements_mapping']['email'] = [
      '#type' => 'select',
      '#title' => $this->t('Email form element'),
      '#description' => $this->t('The form element to be used as email to subscribe an user.'),
      '#options' => $this->getWebformElements(),
      '#default_value' => $this->configuration['email'],
      '#required' => TRUE,
    ];

    $form['elements_mapping']['saludation'] = [
      '#type' => 'details',
      '#title' => $this->t('Saludation'),
    ];

    $form['elements_mapping']['saludation']['default_saludation'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Default saludation text.'),
      '#description' => $this->t('The saludation text will be generated based on the Honorific, Title and last name fields. However in case they are not provided, this text will be the one used.'),
      '#default_value' => $this->configuration['default_saludation'],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();

    $opt_id_is_enabled = !empty($values['mail_list']['opt_id_status']);
    $opt_id_is_set = !empty($values['mail_list']['opt_id']);
    if ($opt_id_is_enabled && !$opt_id_is_set) {
      $form_state->setErrorByName(
        "mail_list][opt_id",
        $this->t('Opt-in ID is required if you want to enable the Opt-in workflow.')
      );
    }

    $conf_checkbox_is_enabled = !empty($values['mail_list']['trigger_by_checkbox']);
    $conf_checkbox_is_set = !empty($values['mail_list']['trigger_checkbox_elem']);
    if ($conf_checkbox_is_enabled && !$conf_checkbox_is_set) {
      $form_state->setErrorByName(
        "mail_list][trigger_checkbox_elem",
        $this->t('Confirmation checkbox is required if you want to trigger the subscription only when a checkbox element is checked.')
      );
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $mail_list_values = $form_state->getValue('mail_list');
    $elem_mapping_values = $form_state->getValue('elements_mapping');

    $this->configuration['auth_code'] = $mail_list_values['auth_code'];
    $this->configuration['opt_in_source'] = $mail_list_values['opt_in_source'];
    $this->configuration['opt_id_status'] = $mail_list_values['opt_id_status'];
    $this->configuration['opt_id'] = $mail_list_values['opt_id'];
    $this->configuration['trigger_by_checkbox'] = $mail_list_values['trigger_by_checkbox'];
    $this->configuration['trigger_checkbox_elem'] = $mail_list_values['trigger_checkbox_elem'];
    $this->configuration['email'] = $elem_mapping_values['email'];
    $this->configuration['default_saludation'] = $elem_mapping_values['saludation']['default_saludation'];
  }

  /**
   * {@inheritdoc}
   */
  public function postSave(WebformSubmissionInterface $webform_submission, $update = TRUE) {
    if ($this->subscriptionIsConfirmed($webform_submission)) {
      try {
        $email = $this->getMappedElementValue('email', $webform_submission);
        $data = $this->getAdditionalSubscriptionData($webform_submission);

        // Subscribe the user into the mail list.
        $response = $this->webformEpiserverManager->subscribe($email, $this->configuration['auth_code'], $data);
      }
      catch (\Exception $exception) {
        // Something went wrong:
        $this->getLogger()->error($exception->getMessage());
        $this->messenger()->addError('Something went wrong. Please contact with the administrators via de contact form.');
      }

      // We customize the error message depending on the code:
      if (!$response['status']) {
        switch ($response['code']) {
          case 'optin_already_started':
            $message = $this->t('A verification email process has been started for this email. Please review your email dashboard to confirm the subscription.');
            $this->messenger()->addError($message);
            break;

          case 'duplicate':
            $message = $this->t('Your email was already subscribed to our newsletter.');
            $this->messenger()->addWarning($message);
            break;

          default:
            $message = $this->t('There were an error on the subscription process. Please try again');
            $this->messenger()->addError($message);
            break;
        }
      }
    }
  }

  /**
   * Retrieves any additional data to be sent to Episerver.
   *
   * @param \Drupal\webform\WebformSubmissionInterface $webform_submission
   *   The Webform Submission entity.
   *
   * @return array
   *   The additional parameters to be sent to the Episerserv instance.
   */
  protected function getAdditionalSubscriptionData(WebformSubmissionInterface $webform_submission) : array {
    $data = [];

    if (!empty($this->configuration['opt_id'])) {
      $data['bmOptInId'] = $this->configuration['opt_id'];
    }

    if (!empty($this->configuration['opt_in_source'])) {
      $data['bmOptinSource'] = $this->configuration['opt_in_source'];
    }

    $saludation = $this->generateSaludation($webform_submission);
    if (!empty($saludation)) {
      $data['salutation'] = $saludation;
    }

    return $data;
  }

  /**
   * Retrieves the Saludation string.
   *
   * @param \Drupal\webform\WebformSubmissionInterface $webform_submission
   *   The Webform submission entity.
   *
   * @return mixed|string
   *   The saludation string.
   */
  protected function generateSaludation(WebformSubmissionInterface $webform_submission) {
    return $this->configuration['default_saludation'];
  }

  /**
   * Checks if the subscription process should be triggered.
   *
   * It takes into account the trigger_by_checkbox config and the mapped
   * trigger_checkbox_elem element.
   *
   * @param \Drupal\webform\WebformSubmissionInterface $webform_submission
   *   The Webform Submission entity.
   *
   * @return bool
   *   TRUE if the subscription process should be triggered. It checks the value
   *   of the trigger_checkbox_elem submitted is checked. It is checked only if
   *   'trigger_by_checkbox' is configured.
   *   By default it returns TRUE.
   */
  protected function subscriptionIsConfirmed(WebformSubmissionInterface $webform_submission) : bool {
    $confirmed = TRUE;

    if ((bool) $this->configuration['trigger_by_checkbox']) {
      $confirmed = (bool) $this->getMappedElementValue(
        'trigger_checkbox_elem',
        $webform_submission
      );
    }

    return $confirmed;
  }

}
