<?php

namespace Drupal\webform_episerver\Plugin\WebformHandler;

use Drupal\webform_episerver\Plugin\SubscriptionWebformHandlerBase;

/**
 * Simple subscription Webform handler.
 *
 * @WebformHandler(
 *   id = "webform_episerver_subscription_simple",
 *   label = @Translation("Episerver Simple subscription"),
 *   category = @Translation("Episerver"),
 *   description = @Translation("Subscribe a user on a mail list from Episerver on submit."),
 *   cardinality = \Drupal\webform\Plugin\WebformHandlerInterface::CARDINALITY_SINGLE,
 *   results = \Drupal\webform\Plugin\WebformHandlerInterface::RESULTS_PROCESSED,
 *   submission = \Drupal\webform\Plugin\WebformHandlerInterface::SUBMISSION_OPTIONAL,
 * )
 */
class SimpleSubscriptionWebformHandler extends SubscriptionWebformHandlerBase {

}
