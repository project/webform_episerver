<?php

namespace Drupal\webform_episerver\Plugin\WebformHandler;

use Drupal\Core\Form\FormStateInterface;
use Drupal\webform\WebformSubmissionInterface;
use Drupal\webform_episerver\Plugin\SubscriptionWebformHandlerBase;

/**
 * Simple subscription Webform handler.
 *
 * @WebformHandler(
 *   id = "webform_episerver_subscription_advanced",
 *   label = @Translation("Episerver Advanced subscription"),
 *   category = @Translation("Episerver"),
 *   description = @Translation("Subscribe a user on a mail list from Episerver on submit."),
 *   cardinality = \Drupal\webform\Plugin\WebformHandlerInterface::CARDINALITY_SINGLE,
 *   results = \Drupal\webform\Plugin\WebformHandlerInterface::RESULTS_PROCESSED,
 *   submission = \Drupal\webform\Plugin\WebformHandlerInterface::SUBMISSION_OPTIONAL,
 * )
 */
class AdvancedSubscriptionWebformHandler extends SubscriptionWebformHandlerBase {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'honorific' => '',
      'title' => '',
      'first_name' => '',
      'last_name' => '',
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['elements_mapping']['first_name'] = [
      '#type' => 'select',
      '#title' => $this->t('First name form element'),
      '#description' => $this->t('The form element to be used as first name to subscribe an user.'),
      '#options' => $this->getWebformElements(),
      '#default_value' => $this->configuration['first_name'],
    ];

    $form['elements_mapping']['last_name'] = [
      '#type' => 'select',
      '#title' => $this->t('Last name form element'),
      '#description' => $this->t('The form element to be used as last name to subscribe an user.'),
      '#options' => $this->getWebformElements(),
      '#default_value' => $this->configuration['last_name'],
    ];

    $form['elements_mapping']['saludation']['honorific'] = [
      '#type' => 'select',
      '#title' => $this->t('Honorific form element'),
      '#description' => $this->t('The form element to be used as honorific to generate the saludation.'),
      '#options' => $this->getWebformElements(),
      '#default_value' => $this->configuration['honorific'],
    ];

    $form['elements_mapping']['saludation']['title'] = [
      '#type' => 'select',
      '#title' => $this->t('Title form element'),
      '#description' => $this->t('The form element to be used as title to generate the saludation.'),
      '#options' => $this->getWebformElements(),
      '#default_value' => $this->configuration['title'],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $elem_mapping_values = $form_state->getValue('elements_mapping');

    $this->configuration['first_name'] = $elem_mapping_values['first_name'];
    $this->configuration['last_name'] = $elem_mapping_values['last_name'];
    $this->configuration['title'] = $elem_mapping_values['saludation']['title'];
    $this->configuration['honorific'] = $elem_mapping_values['saludation']['honorific'];

    parent::submitConfigurationForm($form, $form_state);
  }

  /**
   * Retrieves any additional data to be sent to Episerver.
   *
   * @param \Drupal\webform\WebformSubmissionInterface $webform_submission
   *   The Webform Submission entity.
   *
   * @return array
   *   The additional parameters to be sent to the Episerserv instance.
   */
  protected function getAdditionalSubscriptionData(WebformSubmissionInterface $webform_submission) : array {
    $data = [
      'firstname' => $this->getMappedElementValue('first_name', $webform_submission),
      'lastname' => $this->getMappedElementValue('last_name', $webform_submission),
    ] + parent::getAdditionalSubscriptionData($webform_submission);

    return $data;
  }

  /**
   * {@inheritDoc}
   */
  protected function generateSaludation(WebformSubmissionInterface $webform_submission) {
    $honorific_value = $this->getMappedElementRawValue('honorific', $webform_submission);

    if ($honorific_value !== 'not_specified' && $honorific_value !== 'keine Angabe') {
      // Only build the salutation string in case the honorific has been set
      // (Mr/Mrs)
      $honorific = $this->getMappedElementValue('honorific', $webform_submission);
      $title = $this->getMappedElementValue('title', $webform_submission);
      $last_name = $this->getMappedElementValue('last_name', $webform_submission);

      $salutation_parts = empty($title) ?
        [$honorific, $last_name] : [$honorific, $title, $last_name];

      // Depending on the gender we need to have different contexts to provide
      // the correct translations.
      if ($honorific_value == 'mr') {
        $salutation = $this->t(
          'Dear @salutation_string',
          [
            '@salutation_string' => implode(' ', $salutation_parts),
          ],
          ['context' => 'male']
        );
      }
      else {
        $salutation = $this->t(
          'Dear @salutation_string',
          [
            '@salutation_string' => implode(' ', $salutation_parts),
          ],
          ['context' => 'female']
        );
      }
    }
    else {
      $salutation = parent::generateSaludation($webform_submission);
    }

    return (string) $salutation;
  }

}
