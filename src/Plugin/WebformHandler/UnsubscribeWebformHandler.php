<?php

namespace Drupal\webform_episerver\Plugin\WebformHandler;

use Drupal\webform_episerver\Plugin\UnsubscribeWebformHandlerBase;

/**
 * Unsubscribe Webform handler.
 *
 * @WebformHandler(
 *   id = "webform_episerver_unsubscribe",
 *   label = @Translation("Episerver unsubscribe"),
 *   category = @Translation("Episerver"),
 *   description = @Translation("Unsubscribe a user from a mail list from Episerver on submit."),
 *   cardinality = \Drupal\webform\Plugin\WebformHandlerInterface::CARDINALITY_SINGLE,
 *   results = \Drupal\webform\Plugin\WebformHandlerInterface::RESULTS_PROCESSED,
 *   submission = \Drupal\webform\Plugin\WebformHandlerInterface::SUBMISSION_OPTIONAL,
 * )
 */
class UnsubscribeWebformHandler extends UnsubscribeWebformHandlerBase {

}
