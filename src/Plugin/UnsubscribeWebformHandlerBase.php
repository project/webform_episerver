<?php

namespace Drupal\webform_episerver\Plugin;

use Drupal\Core\Form\FormStateInterface;
use Drupal\webform\Plugin\WebformHandlerBase;
use Drupal\webform\WebformSubmissionInterface;
use Drupal\webform_episerver\WebFormEpiserverElementsMappingTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class UnsubscribeWebformHandlerBase.
 *
 * Provides the base Webform Handler to implement the integration with
 * Episerver.
 */
abstract class UnsubscribeWebformHandlerBase extends WebformHandlerBase {
  use WebFormEpiserverElementsMappingTrait;

  /**
   * The WebformEpiserverManagerInterface service.
   *
   * @var \Drupal\webform_episerver\WebformEpiserverManagerInterface
   */
  protected $webformEpiserverManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->webformEpiserverManager = $container->get('webform_episerver.manager');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'auth_code' => '',
      'email' => '',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form['mail_list'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Mail list settings'),
    ];

    $form['mail_list']['auth_code'] = [
      '#type' => 'key_select',
      '#title' => $this->t('Auth code'),
      '#default_value' => $this->configuration['auth_code'],
      '#required' => TRUE,
    ];

    $form['elements_mapping'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Form elements mapping'),
    ];

    $form['elements_mapping']['email'] = [
      '#type' => 'select',
      '#title' => $this->t('Email form element'),
      '#description' => $this->t('The form element to be used as email to subscribe an user.'),
      '#options' => $this->getWebformElements(),
      '#default_value' => $this->configuration['email'],
      '#required' => TRUE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $elem_mapping_values = $form_state->getValue('elements_mapping');
    $mail_list_values = $form_state->getValue('mail_list');

    $this->configuration['email'] = $elem_mapping_values['email'];
    $this->configuration['auth_code'] = $mail_list_values['auth_code'];
  }

  /**
   * {@inheritdoc}
   */
  public function postSave(WebformSubmissionInterface $webform_submission, $update = TRUE) {
    $email = $this->getMappedElementValue('email', $webform_submission);

    // Blacklist the user into the mail list.
    $response = $this->webformEpiserverManager->blacklist($email, $this->configuration['auth_code']);

    if (!$response['status']) {
      $this->messenger()->addError("There were an error on the un-subscription process. Please try again or contact us via the contact form.");
    }
  }

}
