<?php

namespace Drupal\webform_episerver;

/**
 * Interface WebformEpiserverManagerInterface.
 *
 * Provides methods to subscribe/unsubscribe a recipient into/from a mail_list.
 */
interface WebformEpiserverManagerInterface {

  /**
   * Subscribes an user to the.
   *
   * @param string $email
   *   The email to be subscribed.
   * @param string $auth_code_key
   *   The key of the auth_code to use.
   * @param array $data
   *   Additional and optional data for the subscription.
   *
   * @return array
   *   The response of the operation.
   */
  public function subscribe(string $email, string $auth_code_key, array $data) : array;

  /**
   * Cancels the subscription of an user from the given $mail_list.
   *
   * @param string $email
   *   The email to be unsubscribed.
   * @param string $auth_code_key
   *   The key of the auth_code to use.
   *
   * @return array
   *   The response of the operation.
   */
  public function unsubscribe(string $email, string $auth_code_key) : array;

  /**
   * Removes an user from the blacklist.
   *
   * @param string $email
   *   The email to be unblacklisted.
   * @param string $auth_code_key
   *   The key of the auth_code to use.
   *
   * @return array
   *   The response of the operation.
   */
  public function unblacklist(string $email, string $auth_code_key) : array;

  /**
   * Adds an user to the blacklist.
   *
   * @param string $email
   *   The email to be blacklisted.
   * @param string $auth_code_key
   *   The key of the auth_code to use.
   *
   * @return array
   *   The response of the operation.
   */
  public function blacklist(string $email, string $auth_code_key) : array;

}
