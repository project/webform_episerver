<?php

namespace Drupal\webform_episerver;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\key\KeyRepositoryInterface;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Psr\Log\LoggerInterface;

/**
 * Class WebformEpiserverManager.
 */
class WebformEpiserverManager implements WebformEpiserverManagerInterface {

  /**
   * Drupal\key\KeyRepositoryInterface definition.
   *
   * @var \Drupal\key\KeyRepositoryInterface
   */
  protected $keyRepository;

  /**
   * Guzzle\Client instance.
   *
   * @var \Guzzle\Client
   */
  protected $httpClient;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * A logger instance.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * Constructs a new WebformEpiserverManager object.
   *
   * @param \Drupal\key\KeyRepositoryInterface $key_repository
   *   Drupal\key\KeyRepositoryInterface definition.
   * @param \GuzzleHttp\Client $http_client
   *   The Guzzle\Client instance.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Psr\Log\LoggerInterface $logger
   *   The logger.
   */
  public function __construct(
    KeyRepositoryInterface $key_repository,
    Client $http_client,
    ConfigFactoryInterface $config_factory,
    LoggerInterface $logger
  ) {
    $this->keyRepository = $key_repository;
    $this->httpClient = $http_client;
    $this->configFactory = $config_factory;
    $this->logger = $logger;
  }

  /**
   * {@inheritDoc}
   */
  public function subscribe(string $email, string $auth_code_key, array $data): array {
    $query_params = [
      'bmRecipientId' => $email,
    ] + $data;

    $response = $this->request('subscribe', $auth_code_key, $query_params);

    switch ($response) {
      case 'ok':
      case 'ok: updated':
        return $this->generateSuccessResponse($response);

      case 'blacklisted':
        // The given user has been blacklisted previously:
        // We try to remove it from the blacklist.
        return $this->unblacklist($email, $auth_code_key);

      case 'duplicate':
        $this->logger->error('Subscription failed to due to duplicated email with auth_key: @key. Error: @message', ['@key' => $auth_code_key, '@message' => $response]);
        return $this->generateErrorResponse($response);

      default:
        $this->logger->error('Subscription failed to with auth_key: @key. Error: @message', ['@key' => $auth_code_key, '@message' => $response]);
        return $this->generateErrorResponse($response);
    }
  }

  /**
   * {@inheritDoc}
   */
  public function blacklist(string $email, string $auth_code_key) : array {
    $query_params = [
      'bmPattern' => $email,
    ];

    $response = $this->request('blacklist', $auth_code_key, $query_params);

    switch ($response) {
      case 'ok':
      case 'ok: already_blacklisted':
        return $this->generateSuccessResponse($response);

      default:
        $this->logger->error('Blacklist failed to with auth_key: @key. Error: @message', ['@key' => $auth_code_key, '@message' => $response]);
        return $this->generateErrorResponse($response);
    }
  }

  /**
   * {@inheritDoc}
   */
  public function unblacklist(string $email, string $auth_code_key) : array {
    $query_params = [
      'bmPattern' => $email,
    ];

    $response = $this->request('unblacklist', $auth_code_key, $query_params);

    switch ($response) {
      case 'ok':
        return $this->generateSuccessResponse($response);

      default:
        $this->logger->error('Unblacklist failed to with auth_key: @key. Error: @message', ['@key' => $auth_code_key, '@message' => $response]);
        return $this->generateErrorResponse($response);
    }
  }

  /**
   * {@inheritDoc}
   */
  public function unsubscribe(string $email, string $auth_code_key): array {
    $query_params = [
      'bmRecipientId' => $email,
    ];

    $response = $this->request('unsubscribe', $auth_code_key, $query_params);

    switch ($response) {
      case 'ok':
        return $this->generateSuccessResponse($response);

      default:
        // Something went wrong.
        $this->logger->error('Unsubscription failed to with auth_key: @key. Error: @message', ['@key' => $auth_code_key, '@message' => $response]);
        return $this->generateErrorResponse($response);
    }
  }

  /**
   * Sends a Request against the Episerver API.
   *
   * @param string $service
   *   The service against we perform the request.
   * @param string $auth_code_key
   *   The key of the Auth code to use.
   * @param array $query_params
   *   The query params to be sent.
   *
   * @return string
   *   The response from the service.
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  protected function request(string $service, string $auth_code_key, array $query_params) {
    try {
      $response = $this->httpClient->request(
        'GET',
        $this->getApiUrl() . '/' . $this->getAuthorizationCode($auth_code_key) . '/' . $service,
        [
          'query' => $query_params,
        ]
      );
    }
    catch (GuzzleException $error) {
      $context = [
        '@service' => $service,
        '@error' => $error,
      ];

      $this->logger->error("There were an error with the operation @service, details: @error", $context);
    }
    if (!empty($response)) {
      return $response->getBody()->getContents();
    }
  }

  /**
   * Retrieves the base URL to be used to interact with the API.
   *
   * @return string
   *   The base URL of the Episerver API.
   */
  protected function getApiUrl() : string {
    return $this->configFactory
      ->get('webform_episerver.settings')
      ->get('api_url');
  }

  /**
   * Retrieves the configured Authentication Code.
   *
   * @param string $auth_code_key
   *   The key of the Auth code.
   *
   * @return string
   *   The configured authorization code.
   */
  protected function getAuthorizationCode(string $auth_code_key) : string {
    $auth_code = '';

    if (!empty($auth_code_key)) {
      $auth_code = $this->keyRepository->getKey($auth_code_key)
        ->getKeyValue();
    }

    // We log if the key doesn't exist.
    if (empty($auth_code)) {
      $this->logger->error('Empty Auth Code Key value for the Key: @key', ['@key' => $auth_code_key]);
    }
    return $auth_code;
  }

  /**
   * Generates an error response based on the given $success_code.
   *
   * @param string $success_code
   *   The success code received from the service.
   *
   * @return array
   *   The response with the code and status.
   */
  protected function generateSuccessResponse(string $success_code) : array {
    return [
      'code' => $success_code,
      'status' => TRUE,
    ];
  }

  /**
   * Generates an error response based on the given $error_code.
   *
   * @param string $error_code
   *   The error code received from the service.
   *
   * @return array
   *   The response with the code and status.
   */
  protected function generateErrorResponse(string $error_code) : array {
    return [
      'code' => $error_code,
      'status' => FALSE,
    ];
  }

}
