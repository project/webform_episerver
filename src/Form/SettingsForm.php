<?php

namespace Drupal\webform_episerver\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class SettingsForm.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'webform_episerver.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'webform_episerver_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('webform_episerver.settings');

    $form['api_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Api URL'),
      '#description' => $this->t('The API Url of Episerver. For instance: https://api.campaign.episerver.net/http/form/'),
      '#default_value' => $config->get('api_url'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('webform_episerver.settings')
      ->set('api_url', (string) $form_state->getValue('api_url'))
      ->save();
  }

}
