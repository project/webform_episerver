<?php

namespace Drupal\webform_episerver;

use Drupal\webform\WebformSubmissionInterface;

/**
 * Trait WebFormEpiserverElementsMappingTrait.
 *
 * @package Drupal\webform_episerver
 */
trait WebFormEpiserverElementsMappingTrait {

  /**
   * The webform element manager.
   *
   * @var \Drupal\webform\Plugin\WebformElementManagerInterface
   */
  protected $webformElementManager;

  /**
   * Retrieves the list of Webform elements configured for this webform.
   *
   * @return array
   *   The array of webform Elements configured for this Webform.
   *
   * @throws \Exception
   */
  protected function getWebformElements() : array {
    $element_options = [];

    $elements = $this->webform->getElementsInitializedAndFlattened();
    foreach ($elements as $key => $element) {
      $element_plugin = $this->getWebformElementManager()->getElementInstance($element);
      if ($element_plugin->isInput($element) && isset($element['#type'])) {
        $element_options[$key] = $element['#title'];
      }
    }

    return $element_options;
  }

  /**
   * Gets the email to be sent to Episerver.
   *
   * It gets it from the submitted values depending on the mapped element.
   *
   * @param string $element_key
   *   The $key of the configured element.
   * @param \Drupal\webform\WebformSubmissionInterface $webform_submission
   *   The Webform Submission entity.
   *
   * @return string
   *   The submitted value from the element which matches with $element_key.
   *
   * @throws \Exception
   */
  protected function getMappedElementValue(string $element_key, WebformSubmissionInterface $webform_submission) : string {
    // Get the configured element for the given key:
    $mapped_element_key = $this->configuration[$element_key];

    // Return the value for the mapped element.
    $elem_value = $webform_submission->getElementData($mapped_element_key);

    // The requested mapped $mapped_element_key is not present at the webform
    // elements. We throw an Exception because it might be a something critical.
    if (is_null($elem_value)) {
      throw new \Exception("The mapped $mapped_element_key is not present at the webform elements.");
    }

    if (!empty($element_key)) {
      $elements = $this->webform->getElementsInitializedAndFlattened();

      $element_exists = isset($elements[$mapped_element_key]);
      $elem_has_options = !empty($elements[$mapped_element_key]['#options'][$elem_value]);
      if ($element_exists && $elem_has_options) {
        $elem_value = $elements[$mapped_element_key]['#options'][$elem_value];
      }
    }

    return $elem_value;
  }

  /**
   * Gets the WebformElementManager service.
   *
   * @return \Drupal\webform\Plugin\WebformElementManagerInterface
   *   The WebformElementManager service.
   */
  protected function getWebformElementManager() {
    if (!$this->webformElementManager) {
      $this->webformElementManager = \Drupal::service('plugin.manager.webform.element');
    }

    return $this->webformElementManager;
  }

  /**
   * Gets the raw value from the webformelement given the $element_key.
   *
   * It is really similar to getMappedElementValue() but it doesn't search for
   * the value label.
   *
   * @param string $element_key
   *   The $key of the configured element.
   * @param \Drupal\webform\WebformSubmissionInterface $webform_submission
   *   The Webform Submission entity.
   *
   * @return string
   *   The submitted raw value from the element which matches with $element_key.
   */
  protected function getMappedElementRawValue(string $element_key, WebformSubmissionInterface $webform_submission) : string {
    // Get the configured element for the given key:
    $mapped_element_key = $this->configuration[$element_key];

    // Return the value for the mapped element.
    return $webform_submission->getElementData($mapped_element_key);
  }

}
